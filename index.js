/**
 * Created by acsherrock on 7/29/16.
 */

const ejs        = require('ejs');
const express    = require('express');
const socketio   = require('socket.io');
const unless     = require('express-unless');
const db         = require('./app/db');
const utils      = require('./app/utility');
const ioRoutes   = require('./app/routes/socket');
const httpRoutes = require('./app/routes/http');
const config     = require('./package').config;

const app    = express();
const server = utils.getServer(app);
const io     = socketio(server);


app.set('view engine', 'ejs');
app.use('/static', express.static('static'));

httpRoutes.attach(app, db);
ioRoutes.attach(io, db);

server.listen(config.port, error => {
    if (error) return console.error(`server error: ${error}`);
    if (config.debug) console.log(`listening on ${config.port}`);
});

