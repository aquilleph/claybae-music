/**
 * Created by acsherrock on 7/28/16.
 */
"use strict";

class BaseElement extends HTMLElement {
    create(opts) {
        this.template = opts.script.ownerDocument.getElementById(opts.template);
        let clone = document.importNode(this.template.content, true);

        // Append element to root.
        this.createShadowRoot().appendChild(clone);
    }
}