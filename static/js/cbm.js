/**
 * Created by acsherrock on 7/28/16.
 */

var client = undefined;
var socket = undefined;
var activePage = undefined;

class CBMClient {
    constructor(args) {
        const self = this;
        this.id = undefined;
        this.queue = undefined;
        this.storage = window.localStorage;

        this.loginForm = args.loginForm;
        this.mediaPlayer = args.mediaPlayer;
        this.unauthenticatedRedirect = args.unauthenticatedRedirect || 'login';
        this.accountLinkForms = null;

        socket.on('new-client', this.connectionHandler.bind(this));
        socket.on('track-added', data => console.log(data));

        if (this.loginForm != undefined) {
            let loginSubmit = document.getElementById('submit-plex-login');
            let usernameField = document.getElementsByName('username')[0];
            let passwordField = document.getElementsByName('password')[0];

            loginSubmit.addEventListener('click', _ => {
                this.login({
                    type: 'plex',
                    username: usernameField.value,
                    password: passwordField.value
                });
            });
        }

        this.verifyAuth
            .then(res => {
                this.location = 'account';
                console.log('logged in', res);
            })
            .catch(res => {
                this.location = this.unauthenticatedRedirect;
                console.log('not logged in', res)
            });
    }

    connectionHandler(data) {
        this.id = data.clientId;
        this.accountTypes = data.accountTypes;
        this.queue = new Queue({playbackDevice: this.id});

        // Set event listeners for add-to-queue buttons.
        for (let trackItem of document.getElementsByTagName('track-item')) {
            trackItem.addEventListener('addtrack',    _ => this.addTrackHandler(trackItem));
            trackItem.addEventListener('removetrack', _ => this.removeTrackHandler(trackItem));
        }

        if (this.accountLinkForms === null) {
            // ^ THis is done to make sure this event listener is not assigned twice..
            this.accountLinkForms = document.getElementsByClassName('account-link-form');
            for (let form of this.accountLinkForms) {
                let accountType = this.accountTypes[form.dataset.accountType],
                    submitBtn = form.querySelector('.submit-btn'),
                    usernameField = form.querySelector('.username'),
                    passwordField = form.querySelector('.password');

                submitBtn.addEventListener('click', _ => {
                    console.log('clicked account link submit btn');
                    this.linkAccount({
                        accountType,
                        auth: {
                            email: usernameField.value,
                            password: passwordField.value
                        }
                    });
                });
            }
        }

        if (this.mediaPlayer !== undefined) {
            this.mediaPlayer.addEventListener('statechange', event => {
                if (event.detail.state == player_status.PLAY) {
                    this.queue.play();
                    this.mediaPlayer.state = player_status.PLAY;
                } else {
                    this.queue.pause();
                    this.mediaPlayer.state = player_status.PAUSE;
                }
            });

            this.mediaPlayer.addEventListener('skipnext', this.queue.advance.bind(this.queue));
        }
    }

    getTrackInfo(trackId) {
        return new Promise((resolve, reject) => {
            socket.emit('get-track-info', trackId, (error, response) => {
                if (!!error) reject(error);
                else resolve(response);
            });
        })
    }

    addTrackHandler(trackItem) {
        trackItem.classList.add('in-queue');
        let addTrack = new Promise((resolve, reject) => {
            socket.emit('add-track', trackItem.trackId, (error, response) => {
                if (error) reject(error);
                else resolve(response);
            });
        });
        addTrack
            .catch(console.error.bind(console))
            .then(data => {
                this.getTrackInfo(trackItem.trackId)
                    .catch(console.error.bind(console))
                    .then(response => {
                        this.queue.add(new Track({ info: response }));
                        if (this.queue.length < 2) {
                            this.mediaPlayer.track = this.queue.current;
                        }
                    });
                });
    }

    removeTrackHandler(trackItem) {
        trackItem.classList.remove('in-queue');
        let removeTrack = new Promise((resolve, reject) => {
            socket.emit('remove-song', (error, response) => {
                if (error) reject(response);
                else resolve(response);
            });
        });
    }

    get verifyAuth() {
        return new Promise((resolve, reject) => {
            let token = this.storage.getItem('token');
            if (token === null) {
                reject(false);
            } else {
                socket.emit('verify-authentication', (error, response) => {
                    if (!!error) {
                        reject(error);
                    } else if (response['authenticated'] === false) {
                        reject(false);
                    } else {
                        resolve(response);
                    }
                })
            }
        });
    }

    set location(hash) {
        location.hash = `#${hash}`;
    }

    login(args) {
        socket.emit('login', args, (error, response) => {
            if (!!error) return console.error(error);

            // TODO: Store tokens, etc.
            this.storage.setItem('authenticated', true);
            this.storage.setItem('token', response);
            this.location = 'account';

            // TODO: do better withe this.
            console.log(response);
        });
    }

    linkAccount(args) {
        args['token'] = this.storage.getItem('token');
        console.log('link-account message emitting.')
        socket.emit('link-account', args, (error, response) => {
            if (!!error) return console.error(error);

            console.log(response);
        });
    }
}


class Queue {
    constructor(args) {
        this._playbackDevice = args.playbackDevice;
        this.queue = [];
        this.history = [];
    }

    get length() { return this.queue.length }
    get empty()  { return this.length < 1 }
    get hasNext() { return this.length > 1 }
    get playbackDevice() { return this._playbackDevice }

    get current() {
        if (!this.empty)
            return this.queue[0];
        return false;
    }

    get next() {
        if (this.hasNext) return this.queue[1]
    }

    add(track) {
        this.queue.push(track);
        this.current.onend = this.advance;
        // this.current.onending = this.next.requestStreamUrl.bind(this.next);

        if (this.length < 2) {
            this.current.requestStreamUrl();
        }
    }

    advance() {
        console.log(this.current);

        if (this.current.audio != undefined)
            this.current.pause();

        this.history.push(this.queue.shift());
        this.current.play();
        console.log(this.current);
    }

    previous() {
        this.queue.unshift(this.history.pop());
        this.current.play();
    }

    pause() {
        if (this.empty) return;
        this.current.pause();
    }

    play() {
        if (this.empty) return;
        this.current.play();
    }

    currentTrackEndingHandler() {
        if (this.hasNext)
            this.next.requestStreamUrl();
    }
}


class Track {
    constructor(args) {
        this.info     = args.info;
        this.id       = args.info.id;
        this.title    = args.info.title;
        this.artist   = args.info.artist;
        this.album    = args.info.album;
        this.duration = args.info.duration;

        this.preload = args.preload || true;
        this.audio = new Audio();

        // Time in seconds that a gpm stream url will remain valid
        this.gpmStreamUrlExpiration = 50;
        this.gpmStreamUrlReceivedTime = -1;

        this.audio.ontimeupdate = evt => {
            if (this.duration - this.audio.currentTime < 20) {
                this.audio.dispatchEvent(new CustomEvent('ending', {
                    cancelable: true,
                    bubbles: true
                }));

            }
        }
    }

    requestStreamUrl() {
        return new Promise((resolve, reject) => {
            if (this.preload && this.hasValidStreamUrl) {
                resolve('alreadyloaded');
            } else {
                socket.emit('request-stream-url', this.id, (error, response) => {
                    if (!!error) {
                        reject(error);
                    } else {
                        this.gpmStreamUrlReceivedTime = this.currentSeconds;
                        this.audio.src = response;
                        if (this.preload) {
                            this.audio.load();
                        }

                        resolve(response);
                    }
                })
            }
        })
    }


    pause() {
        this.audio.pause();
        return this;
    }

    play() {
        if (this.hasValidStreamUrl) {
            this.audio.play();
        } else {
            this.requestStreamUrl()
                .catch(console.error.bind(console))
                .then(res => {
                    this.audio.play()
                });
        }

        return this;
    }

    set onend(fn) { this.audio.addEventListener('ended', fn) }
    set onending(fn) { this.audio.addEventListener('ending', fn) }
    get hasReceivedGPMStreamUrl() {
        return this.gpmStreamUrlReceivedTime !== -1;
    }
    get streamUrlExpired() {
        return !this.preload || (this.currentSeconds - this.gpmStreamUrlReceivedTime >= this.gpmStreamUrlExpiration)
    }
    get hasValidStreamUrl() {
        return this.hasReceivedGPMStreamUrl && !this.streamUrlExpired;
    }
    get currentSeconds() {
        return (new Date()).getSeconds();
    }
}


window.onload = function () {
    pageChangeHandler();
    window.onhashchange = pageChangeHandler;

    socket = io('//' + document.domain + ':' + location.port);

    let loginForm = document.getElementById('login-form');
    let mediaPlayerTmp = document.getElementsByTagName('media-player');
    let mediaPlayer = (mediaPlayerTmp.length > 0)
        ? mediaPlayerTmp[0]
        : undefined;

    client = new CBMClient({
        loginForm, mediaPlayer
    });
};


function pageChangeHandler() {
    let hash = location.hash;
    let newActivePage = undefined;

    if (hash !== undefined && hash !== "") {
        newActivePage = document.getElementById(hash.substring(1));
    } else {
        newActivePage = document.querySelector('.page.main')
    }

    if (activePage !== undefined) {
        activePage.classList.remove('active');
    }

    if (!!newActivePage) {
        activePage = newActivePage;
        activePage.classList.add('active');
    }
}
