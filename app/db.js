/**
 * Created by acsherrock on 8/28/16.
 */

const config = require('../package').config;
const objection = require('objection');
const Knex = require('knex');
const Model = objection.Model;
// const models = require('./models');

let knex = Knex({
    client: 'postgresql',
    connection: {
        host: config.db.host,
        user: config.db.user,
        database: config.db.name,
        charset: 'utf-8'
    }
});

Model.knex(knex);

module.exports = {
    objection,
    models: require('./models')(Model)
};