/**
 * Created by acsherrock on 7/29/16.
 */

const config = require('../package').config
const GPMusic = require('playmusic');

const gpm = new GPMusic();

function requestLibrary(fn) {
    gpm.init({
        email: "acsherrock@gmail.com",
        password: "ajlhsukoqxgdtldc"
    }, gpmAuthenticationHandler);


    function gpmAuthenticationHandler(error) {
        if (!!error) return console.error(error);
        gpm.getLibrary({limit: 20}, libraryResponseHandler)
    }

    function libraryResponseHandler(error, library) {
        if (error) return console.error(error);

        let dict = {};
        for (let track of library.data.items) {
            dict[track.id] = track;
        }

        fn({
            dict,
            arr: library.data.items
        })
    }
}

function getStreamUrl(id, fn) {
    gpm.getStreamUrl(id, fn);
}


/**
 * GPM Login.
 * Login to GPM and receive a master token and an androidID upon Promise
 * resolution. Supply argument object with auth object containing an email and
 * password and an optional androidId.
 *
 * @param args: { auth: {email, password}, androidId: "string" }
 *
 */
function login(args) {
    const GPM = new GPMusic();
    return new Promise((resolve, reject) => {
        GPM.login(args, (error, response) => {
            if (!!error) return reject(error);
            resolve(response);
        });
    });
}


/**
 * Get GPM Library.
 * @param data
 * @returns {Promise}
 */
function getLibrary(data) {
    return new Promise((resolve, reject) => {
        let GPM = new GPMusic();
        GPM.init({
            androidId: data.androidId,
            masterToken: data.masterToken
        }, (error, response) => {
            if (error) reject(error);

            GPM.getAllTracks(data.opts, (error, response) => {
                if (!!error) reject(error);

                let trackListOnly = false;
                if (trackListOnly) {
                    resolve({
                        library: response.data.items,
                        gpmSession: GPM
                    });
                } else {
                    resolve({
                        library: response,
                        gpmSession: GPM
                    });
                }
            });
        });
    });
}

function getAlbum(data) {
    return new Promise((resolve, reject) => {
        let GPM = new GPMusic();
        GPM.init({
            androidId: data.androidId,
            masterToken: data.masterToken
        }, error => {
            if (error) reject(error);
            GPM.getAlbum(data.gpm_albumId, false, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        });
    })
}

function getAlbum2(args) {
    return new Promise((resolve, reject) => {
        args.gpm.getAlbum(args.gpm_albumId, false, (error, response) => {
            if (!!error) reject(error);
            resolve(response);
        });
    });
}

function getArtist(data) {
    return new Promise((resolve, reject) => {
        let GPM = new GPMusic();
        GPM.init({
            androidId: data.androidId,
            masterToken: data.masterToken
        }, error => {
            console.log(data);
            if (error) reject(error);
            GPM.getArtist(data.gpm_artistId, false, 0, 0, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        });
    })
}

function getArtist2(args) {
    return new Promise((resolve, reject) => {
        args.gpm.getArtist(args.gpm_artistId, false, 0, 0, (error, response) => {
            if (!!error) reject(error);
            resolve(response);
        });
    })
}


module.exports = {
    getLibrary,
    getAlbum,
    getArtist,
    getAlbum2,
    getArtist2,
    getStreamUrl,
    login
}