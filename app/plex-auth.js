/**
 * Created by acsherrock on 8/7/16.
 */


const request = require('request');
const xml2js  = require('xml2js');

const PLEX_HOST = 'https://plex.tv';


const plexHeaders = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'X-Plex-Product': 'Plex Media Server',
    'X-Plex-Version': '1.0',
    'X-Plex-Client-Identifier': '1234567890'
};

function signin(data) {
    return new Promise((resolve, reject) => {
        request.post(`${PLEX_HOST}/users/sign_in.json`, {
            headers: plexHeaders,
            auth: {
                user: data.username,
                pass: data.password
            }
        }, (error, _, body) => {
            if (!!error) return reject(error);

            // Handle and return useful data.
            let json = JSON.parse(body);
            let user = json['user'];
            resolve({
                plex_userid: user.id,
                plex_uuid: user.uuid,
                plex_authToken: user.authToken,
                plex_username: user.username
            });
        });
    });
}


/**
 *
 * @param data
 * @returns {Promise}
 */
function getfriends(data) {
    return new Promise((resolve, reject) => {
        request.get(`${PLEX_HOST}/pms/friends/all`, {
            auth: {
                user: data.username,
                pass: data.password
            },
            headers: {
                'X-Plex-Product': 'Plex Media Server',
                'X-Plex-Version': '1.0',
                'X-Plex-Client-Identifier': '1234567890'
            }
        }, (error, res, body) => {
            if (!!error) return reject(error);

            // Handle data if no errors.
            xml2js.parseString(body, (error, result) => {
                if (!!error) return reject(error);

                let users = result['MediaContainer']['User'];
                let users_clean = users.map(user => user['$']);
                resolve(users_clean, body);
            });
        });
    });
}

module.exports = {
    signin, getfriends
};