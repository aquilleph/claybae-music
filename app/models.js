/**
 * Created by acsherrock on 9/5/16.
 */

// const Model = require('objection').Model;

module.exports = function (Model) {
    class Album extends Model {
        static get tableName() {
            return 'tb_album'
        }

        static get relationalMappings() {
            return {
                artist: {
                    relation: Model.BelongsToOneRelation,
                    modelClass: Artist,
                    join: {
                        from: 'tb_album.artist_id',
                        to: 'tb_artist.id'
                    }
                },

                songs: {
                    relation: Model.HasManyRelation,
                    modelClass: Song,
                    join: {
                        from: 'tb_album.id',
                        to: 'tb_song.album_id'
                    }
                }
            }
        }

        static get jsonSchema() {
            return {
                type: 'object',
                required: ['name', 'artist_id'],
                properties: {
                    id:        { type: 'integer' },
                    name:      { type: 'string' },
                    artist_id: { type: 'integer' },
                    artists:   { type: 'array' },
                    year:      { type: 'integer' },
                    art:       { type: 'object' },
                    gpm_id:    { type: 'string' },
                }
            }
        }
    }

    class Artist extends Model {
        static get tableName() {
            return 'tb_artist'
        }

        static get relationalMappings() {
            return {
                albums: {
                    relation: Model.HasManyRelation,
                    modelClass: Album,
                    join: {
                        from: 'tb_artist.id',
                        to: 'tb_album.artist_id'
                    }
                },

                songs: {
                    relation: Model.HasManyRelation,
                    modelClass: Song,
                    join: {
                        from: 'tb_artist.id',
                        to: 'tb_song.artist_id'
                    }
                }
            }
        }

        static get jsonSchema() {
            return {
                type: 'object',
                required: ['name'],
                properties: {
                    id:              { type: 'integer' },
                    name:            { type: 'string' },
                    bio:             { type: 'string' },
                    bio_attribution: { type: 'object' },
                    art:             { type: 'object' },
                    gpm_id:          { type: 'string' },
                }
            }
        }
    }

    class Song extends Model {
        static get tableName() {
            return 'tb_song'
        }

        static get relationalMappings() {
            return {
                album: {
                    relation: Model.BelongsToOneRelation,
                    modelClass: Album,
                    join: {
                        from: 'tb_song.album_id',
                        to: 'tb_album.id'
                    }
                },

                artist: {
                    relation: Model.BelongsToOneRelation,
                    modelClass: Artist,
                    join: {
                        from: 'tb_song.artist_id',
                        to: 'tb_artist.id'
                    }
                }
            }
        }

        static get jsonSchema() {
            return {
                type: 'object',
                required: [
                    'title',
                    'artist_id',
                    'album_id'
                ],
                properties: {
                    id:           { type: 'integer' },
                    title:        { type: 'string' },
                    artists:      { type: 'array' },
                    artist_id:    { type: 'integer' },
                    album_id:     { type: 'integer' },
                    album_artist: { type: 'string' },
                    year:         { type: 'integer' },
                    size:         { type: 'integer' },
                    duration:     { type: 'integer' },
                    track_number: { type: 'integer' },
                    disk_number:  { type: 'integer' },
                    composer:     { type: 'string' },
                    genre:        { type: 'string' },
                    art:          { type: 'object' },
                    gpm_id:       { type: 'string' },
                    gpm_store_id: { type: 'string' },
                }
            }
        }
    }

    class SongSource extends Model {
        static get tableName() {
            return 'tb_song_source'
        }
    }

    class Source extends Model {
        static get tableName() {
            return 'tb_source'
        }
    }

    class User extends Model {
        static get tableName() { return 'tb_user' }

        static get jsonSchema() {
            return {
                type: 'object',
                required: ['email'],
                properties: {
                    id:               { type: 'integer' },
                    email:            { type: 'string' },
                    username:         { type: 'string' },
                    gpm_login:        { type: 'string' },
                    gpm_master_token: { type: 'string' },
                    android_id:       { type: 'string' },
                    plex_userid:      { type: 'integer' },
                    plex_uuid:        { type: 'string' },
                    plex_email:       { type: 'string' },
                    plex_username:    { type: 'string' },
                    verified:         { type: 'boolean' },
                    created:          { type: 'string' },
                    last_update:      { type: 'string' }
                }
            }
        }
    }

    class UserSong extends Model {
        static get tableName() {
            return 'tb_user_song'
        }
    }

    return {
        Album,
        Artist,
        Song,
        Source,
        User,
        UserSong
    }
}


