/**
 * Created by acsherrock on 8/28/16.
 */

const config = require('../package').config;



/**
 *
 * @param options:
 *           optionsOnly: exclude execution path and JS file path.
 *           parseKwargs: parse arguments into a dictionary. Non KW arguments
 *                        will have their index as the key.
 * @param fn: callback function called with each command line argument as
 * an argument.
 */
let parseArgs = function(options, fn) {
    if (typeof options === 'function') {
        fn = options;
        options = {};
    }

    options['optionsOnly'] = options.optionsOnly || true;
    options['parseKwargs'] = options.parseKwargs || true;

    let args = (options.optionsOnly)
        ? process.argv.splice(2)
        : process.argv;

    if (options.parseKwargs) {
        args.forEach((arg, index) => {
            let [k,v] = arg.split('=');
            if (v === "undefined") {
                v = k;
                k = index;
            } else {
                while (k.indexOf('-') == 0) {
                    k = k.slice(1);
                }
            }

            if (v.toLowerCase() === "true") {
                v = true;
            } else if (v.toLowerCase() === "false") {
                v = false;
            }

            let argObj = {};
            argObj[k] = v;
            fn(argObj);
        });
    } else {
        for (let arg of args) {
            fn(arg);
        }
    }
}

let useSSL = function () {
    /* TODO: finish this async probs. spending too much time on this bc adderall */
    if (config.use_ssl) return true;
    parseArgs({parseKwargs: true}, arg => {
        console.log(arg);
        if (arg.ssl) {
            console.log('using ssl');
        }
    })
}

let getServer = function(application) {
    if (config.use_ssl) {
        const fs = require('fs');
        const https = require('https');
        const cert = {
            key: fs.readFileSync(config.ssl_key_path),
            cert: fs.readFileSync(config.ssl_cert_path)
        };

        return https.createServer(cert, application);
    } else {
        const http = require('http');
        return http.createServer(application);
    }
}


module.exports = {
    getServer,
    parseArgs
}