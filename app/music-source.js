/**
 * Created by acsherrock on 8/28/16.
 */

const accountType = {
    gpm: 1,
    spotify: 2,
    youtube: 3
};


module.exports = {
    accountType: accountType
};