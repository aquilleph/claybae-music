/**
 * Created by acsherrock on 9/8/16.
 */

let DB = null;

let index = function (req, res) {
    res.render('profile')
}

module.exports.attach = function (app, db) {
    DB = db;

    app.get('/', index);
}