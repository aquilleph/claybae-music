/**
 * Created by acsherrock on 9/8/16.
 */

const jwt      = require('jsonwebtoken');
const config   = require('../../package').config;
const gpm      = require('../library');
const plexAuth = require('../plex-auth');
const music    = require('../music-source');
const importer  = require('../importer');

let IO = null;
let DB = null;
let models = null;

/**
 * Login.
 *
 * Authenticate user with their plex credentials. Save Login email and username
 * but no passwords. Just user plex api for authentication for now.
 *
 * @param data: object containing login info.
 * @param fn: callback for socket response.
 */
let login = function (data, fn) {
    if (data.type === 'plex') {
        plexAuth.signin(data)
            .catch(fn)
            .then(response => {
                console.log(response);
                models.User
                    .query()
                    .where('plex_uuid', response.plex_uuid)
                    .catch(fn)
                    .then(response => {
                        let user = response[0];
                        if (user === undefined) {
                            console.log('new user')
                            // User is not yet in CBM database.
                            models.User
                                .query()
                                .insert({
                                    email: data.username,
                                    username: response.plex_username,
                                    plex_userid: response.plex_userid,
                                    plex_uuid: response.plex_uuid,
                                    plex_username: response.plex_username,
                                })
                                .catch(e => console.error(e))
                                .then(user => fn(false, jwt.sign({
                                    uid: user.id,
                                    email: user.email,
                                    username: user.username
                                }, 'oiuy98Yssad&Tguig897GU*O&')));

                        } else {
                            // User already exists.
                            fn(false, jwt.sign({
                                uid: user.id,
                                email: data.username,
                                username: response.username
                            }, 'oiuy98Yssad&Tguig897GU*O&'));
                        }
                    })
            });
    }
}

/**
 * Link Account.
 *
 * Link user's external GPM, Spotify, etc account. Save login email/username
 * and master tokens.
 *
 * @param data: object containing login info for account.
 * @param fn: callback for socket response.
 */
let linkAccount = function (data, fn) {
    // TODO: switch to google 2-factor auth api for 2-factor users.
    switch (data.accountType) {
        case music.accountType.gpm:
            gpm.login(data.auth)
                .catch(fn)
                .then(response => {
                    let token = jwt.decode(data.token);

                    models.User
                        .query()
                        .findById(token.uid)
                        .catch(err => fn(true, err))
                        .then(user => {
                            if (user.gpm_login === null) {
                                models.User
                                    .query()
                                    .update({
                                        // ajlhsukoqxgdtldc
                                        android_id: response.androidId,
                                        gpm_master_token: response.masterToken,
                                        gpm_login: data.auth.email
                                    })
                                    .where('id', token.uid)
                                    .catch(err => fn(true, err))
                                    .then(res => fn(false, res));
                            }
                        });

                    importer.GPM({
                        androidId: response.androidId,
                        masterToken: response.masterToken
                    });
                });
            break;
        case music.accountType.spotify:
        case music.accountType.youtube:
        default:
            fn('spotify and youtube not yet supported.');
            break;
    }
}

/**
 * Stream URI Request Handler.
 * @param trackId: ID of the track retrieve.
 * @param fn: callback function for socket response.
 */
let requestStreamURL = function (trackId, fn) {
    // TODO: take object as first param, with id and idType.
    gpm.getStreamUrl(trackId, fn)
}

/**
 * Stream URI Request Handler.
 * @param trackId: ID of the track retrieve.
 * @param fn: callback function for socket response.
 */
let getTrackInfo = function (trackId, fn) {
    // fn(false, library.dict[trackId])
    fn('get-track-info not currently implemented.');
}

/**
 * Add Track.
 *
 * Add a track to a queue.
 *
 * @param trackId: ID of track to add to queue.
 * @param fn: callback for socket response.
 */
let addTrack = function (trackId, fn) {
    fn('add-track not currently implemented.');
    // queue.push(library.dict[id]);
    // fn(false, queue);
    //
    // this.emit('track-added', queue);
}

module.exports.attach = function (io, db) {
    IO = io;
    DB = db;
    models = db.models;

    io.on('connection', socket => {
        if (config.debug)
            console.log(`new connection with socket id ${socket.id}`);

        socket.emit('new-client', {
            clientId: socket.id,
            accountTypes: music.accountType
        });

        socket.on('login', login);
        socket.on('link-account', linkAccount);
        socket.on('get-track-info', getTrackInfo);
        socket.on('request-stream-url', requestStreamURL);
        socket.on('add-track', addTrack);
    });
}