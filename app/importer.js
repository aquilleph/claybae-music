/**
 * Created by acsherrock on 9/5/16.
 */


const gpm = require('./library');
const models = require('./db').models;
const Artist = models.Artist,
      Album  = models.Album,
      Song   = models.Song;


function syncronousListLoop(list, fn) {
    if (list.length > 0) {
        let item = list.shift();

        fn(item)
            .catch(console.error.bind(console))
            .then(res => {
                console.log(res.id);
                syncronousListLoop(list, fn);
            });
    }
}

function GPM(args) {
    args.opts = {
        limit: 2000,
        // nextPageToken: ''
    }

    gpm.getLibrary(args)
        .catch(console.error.bind(console))
        .then(response => {
            let library = response.library.data.items;
            console.log(response.library.nextPageToken);
            args.gpm = response.gpmSession;


            syncronousListLoop(library.reverse(), track => {
                return new Promise((resolve, reject) => {
                    args.gpm_artistId = track.artistId[0];
                    args.queryColumn = 'gpm_id';
                    args.queryValue = args.gpm_artistId;

                    createArtist(args)
                        .catch(reject)
                        .then(artist => {
                            args.artistId = artist.id;
                            args.gpm_albumId = track.albumId;
                            args.queryValue = args.gpm_albumId;

                            createAlbum(args)
                                .catch(reject)
                                .then(album => {
                                    args.albumId = album.id;
                                    args.track = track;
                                    args.gpm_songId = track.id;
                                    args.queryValue = args.gpm_songId;

                                    createSong(args)
                                        .catch(reject)
                                        .then(resolve)
                                })
                        })
                })
            }, done => console.log(done));
        });
}

function getDBArtist(args) {
    return new Promise((resolve, reject) => {
        Artist
            .query()
            .where(args.queryColumn, args.queryValue)
            .catch(reject)
            .then(artist => {
                if (artist[0] == undefined) {
                    resolve({ exists: false })
                } else {
                    resolve({
                        exists: true,
                        id: artist[0].id
                    })
                }
            })
    })
}

function getDBAlbum(args) {
    return new Promise((resolve, reject) => {
        Album
            .query()
            .where(args.queryColumn, args.queryValue)
            .catch(reject)
            .then(album => {
                if (album[0] == undefined) {
                    resolve({ exists: false })
                } else {
                    resolve({
                        exists: true,
                        id: album[0].id
                    })
                }
            })
    })
}

function getDBSong(args) {
    return new Promise((resolve, reject) => {
        Song
            .query()
            .where(args.queryColumn, args.queryValue)
            .catch(reject)
            .then(song => {
                if (song[0] == undefined) {
                    resolve({ exists: false })
                } else {
                    resolve({
                        exists: true,
                        id: song[0].id
                    })
                }
            })
    })
}

function createArtist(args) {
    return new Promise((resolve, reject) => {
        getDBArtist(args)
            .catch(reject)
            .then(db_artist => {
                if (db_artist.exists === true) {
                    resolve(db_artist);
                } else {
                    gpm.getArtist2(args)
                        .catch(reject)
                        .then(gpm_artist => {
                            Artist
                                .query()
                                .insert({
                                    name: gpm_artist.name,
                                    art: {
                                        url: gpm_artist.artistArtRef
                                    },
                                    bio: gpm_artist.artistBio,
                                    gpm_id: gpm_artist.artistId,
                                    bio_attribution: {
                                        source_title:  gpm_artist.artist_bio_attribution['source_title'],
                                        source_url:    gpm_artist.artist_bio_attribution['source_url'],
                                        license_title: gpm_artist.artist_bio_attribution['license_title'],
                                        license_url:   gpm_artist.artist_bio_attribution['license_url']
                                    }
                                })
                                .catch(reject)
                                .then(artist => {
                                    console.log(`Artist ${artist.name} added to db`);
                                    resolve(artist);
                                });
                        });
                }
            });
    })
}

function createAlbum(args) {
    return new Promise((resolve, reject) => {
        getDBAlbum(args)
            .catch(reject)
            .then(db_album => {
                if (db_album.exists === true) {
                    resolve(db_album);
                } else {
                    gpm.getAlbum2(args)
                        .catch(reject)
                        .then(gpm_album => {
                            Album
                                .query()
                                .insert({
                                    artist_id: args.artistId,
                                    name: gpm_album.name,
                                    art: {
                                        url: gpm_album.albumArtRef
                                    },
                                    gpm_id: gpm_album.albumId,
                                    year: gpm_album.year
                                })
                                .catch(reject)
                                .then(album => {
                                    console.log(`Album ${album.name} added to db`);
                                    resolve(album);
                                });
                        });
                }
            });
    })
}

function createSong(args) {
    return new Promise((resolve, reject) => {
        getDBSong(args)
            .catch(reject)
            .then(db_song => {
                if (db_song.exists === true) {
                    resolve(db_song);
                } else {
                    Song
                        .query()
                        .insert({
                            title: args.track.title,
                            album_artist: args.track.albumArtist,
                            artist_id: args.artistId,
                            artists: args.track.artistId,
                            album_id: args.albumId,
                            gpm_id: args.track.id,
                            gpm_store_id: args.track.storeId,
                            year: args.track.year,
                            disk_number: args.track.discNumber,
                            track_number: args.track.trackNumber,
                            size: args.track.estimatedSize,
                            duration: args.track.durationMillis,
                            genre: args.track.genre,
                            composer: args.track.composer
                        })
                        .catch(reject)
                        .then(song => {
                            console.log(`Song ${song.title} song added to db`);
                            resolve(song)
                        });
                }
            });
    })
}

module.exports = {
    GPM
};